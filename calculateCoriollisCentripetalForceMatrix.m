function C = calculateCoriollisCentripetalForceMatrix(nu, m, r_g, I_b)
            
            v2 = nu(4:6);            
            
            Smtrx_v2 = [0 -v2(3) v2(2); ... 
                       v2(3) 0 -v2(1); ... 
                       -v2(2) v2(1) 0];
            
            I_bv2 = I_b*v2;
                  
            Smtrx_I_bv2 = [0 -I_bv2(3) I_bv2(2); ... 
                           I_bv2(3) 0 -I_bv2(1); ... 
                          -I_bv2(2) I_bv2(1) 0];
            
            Smtrx_r_g = [0 -r_g(3) r_g(2); ... 
                         r_g(3) 0 -r_g(1); ... 
                         -r_g(2) r_g(1) 0];
                   
            C11 = m*Smtrx_v2;
            C12 = -m*Smtrx_v2*Smtrx_r_g;
            C21 = m*Smtrx_r_g*Smtrx_v2;
            C22 = -Smtrx_I_bv2;
            
            C = [C11 C12; ...
                 C21 C22];
            
end