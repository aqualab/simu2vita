 function g = calculateBouyaneForces(cog, cob, eta, B, W)
    
    coder.extrinsic('MException')
    
    ori = eta(4:7);
    
    g = zeros(6,1);
    
    [Mori, Nori] = size(ori);
    
    if(Mori == 3 && Nori == 1)
        
    elseif(Mori == 4 && Nori == 1)

         inv_ori = [eta(4); -eta(5:end)]/norm(eta(4:end));
%        
%         tmp_vec = kronnecker_quat(inv_ori, [0;0;0;W]);                    
%         weight_wrt_body = kronnecker_quat(tmp_vec, eta(4:end));
%         
%         tmp_vec = kronnecker_quat(inv_ori, [0;0;0;B]);
%         bouyancy_wrt_body = kronnecker_quat(tmp_vec, eta(4:end));

        weight_wrt_body = quat_rot(inv_ori, [0;0;W]);
        bouyancy_wrt_body = quat_rot(inv_ori, [0;0;B]);
        
        g(1:3) = -(weight_wrt_body + bouyancy_wrt_body);
        g(4:6) = -(cross(cog, weight_wrt_body) + cross(cob, bouyancy_wrt_body));
         
    else

%         g={};

%         ME = MException('MyComponent:incompatibleOrientation', ...
%                 'Orientation vector does not match a 3x1(RPY) or a 4x1(quat) vector!');
% 
%         throw(ME);

    end

end
