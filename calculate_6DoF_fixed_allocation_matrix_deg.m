function Alloc_Matrix = calculate_6DoF_fixed_allocation_matrix_deg(thrusters_orientation, thrusters_position)
    
    number_of_thrusters = size(thrusters_orientation,1);
    
    thrusters_ori_rad = deg2rad(thrusters_orientation);
    
    Alloc_Matrix = zeros(6, number_of_thrusters);
    
    DCM = angle2dcm(thrusters_ori_rad(:,1), thrusters_ori_rad(:,2), thrusters_ori_rad(:,3), 'XYZ');
    
    for i=1:1:number_of_thrusters
       
        Alloc_Matrix(1:3, i) = DCM(:,1,i);
        Alloc_Matrix(4:6, i) = cross(thrusters_position(i,:), Alloc_Matrix(1:3, i));
        
    end
    
end