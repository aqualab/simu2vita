# Simu2VITA

A simple simulator built on MATLAB and Simulink framework and inspired by other Matlab blocks.
We aimed for simple and rapid prototipation with good physics simulation. Simu2VITA simulate 
actuators and the dynamics of the rigid body of the vehicle.