function rot_vec = quat_rot (q, r)
    
    S = [0 -q(4) q(3); ...
         q(4) 0 -q(2); ...
         -q(3) q(2) 0;];

    rot_vec = (q(1) - norm(q(2:4)))*r + 2*q(2:4)*(q(2:4)'*r) + 2*q(1)*S*r;
    
end