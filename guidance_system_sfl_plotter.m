close all;
set(0,'defaulttextinterpreter','latex')

dir_to_save_figs = '/home/spades/VITA_Project/articles/sensors_article_simu2vita/sections/experiments/figs/'

rpy_ori = out.orientation_deg.Data;

roll_lims = get_intervals_to_limit_plot(rpy_ori(:,1), 2, 3);
pitch_lims = get_intervals_to_limit_plot(rpy_ori(:,2), 2, 3);
yaw_lims = get_intervals_to_limit_plot(rpy_ori(:,3), 2, 3);
    
fig_ori = figure('units','normalized','outerposition',[0 0 1 1]);
set(fig_ori, 'Visible', 'off');
subplot(3,1,1);
plot(out.orientation_deg.Time, rpy_ori(:,1), 'g', 'LineWidth', 3);
set(gca, 'FontSize', 28);
grid on;
xlabel('time [s]','FontSize',28);
ylabel('Angle [degree]','FontSize',20);
title('Roll Angle Evolution in time','FontSize',36);
ylim([roll_lims(1) roll_lims(end)])
yticks(roll_lims)

subplot(3,1,2);
plot(out.orientation_deg.Time, rpy_ori(:,2), 'b', 'LineWidth', 3);
set(gca, 'FontSize', 28);
grid on;
xlabel('time [s]','FontSize',28);
ylabel('Angle [degree]','FontSize',20);
title('Pitch Angle Evolution in time','FontSize',36);
ylim([pitch_lims(1) pitch_lims(end)])
yticks(pitch_lims)

subplot(3,1,3);
plot(out.orientation_deg.Time, rpy_ori(:,3), 'r', 'LineWidth', 3);
set(gca, 'FontSize', 28);
grid on;
xlabel('time [s]','FontSize',28);
ylabel('Angle [degree]','FontSize',20);
title('Yaw Angle Evolution in time','FontSize',36);
ylim([yaw_lims(1) yaw_lims(end)])
yticks(yaw_lims)

print(fig_ori, [dir_to_save_figs 'simu2vita_ori_evolution.svg'], "-dsvg");

pos_ned = out.eta_position.Data;

n_lims = get_intervals_to_limit_plot(pos_ned(:,1), 2, 3);
e_lims = get_intervals_to_limit_plot(pos_ned(:,2), 2, 3);
d_lims = get_intervals_to_limit_plot(pos_ned(:,3), 2, 3);

fig_pos = figure('units','normalized','outerposition',[0 0 1 1]);
set(fig_pos, 'Visible', 'off');
subplot(3,1,1);
plot(out.eta_position.Time, pos_ned(:,1), 'g', 'LineWidth', 3);
set(gca, 'FontSize', 28);
grid on
xlabel('time [s]','FontSize',28);
ylabel('Position [m]','FontSize',28);
title('$n$ component of ${}^{w}\mathbf{p}_{b}$ evolution over time','FontSize',36, 'Interpreter','latex');
ylim([n_lims(1) n_lims(end)])
yticks(n_lims)


subplot(3,1,2);
plot(out.eta_position.Time, pos_ned(:,2), 'b', 'LineWidth', 3);
set(gca, 'FontSize', 28);
grid on;
xlabel('time [s]','FontSize',28);
ylabel('Position [m]','FontSize',28);
title('$e$ component of ${}^{w}\mathbf{p}_{b}$ evolution over time','FontSize',36, 'Interpreter','latex');
ylim([e_lims(1) e_lims(end)])
yticks(e_lims)

subplot(3,1,3);
plot(out.eta_position.Time, pos_ned(:,3), 'r', 'LineWidth', 3);
set(gca, 'FontSize', 28);
grid on;
xlabel('time [s]','FontSize',28);
ylabel('Position [m]','FontSize',28);
title('$d$ component of ${}^{w}\mathbf{p}_{b}$ evolution over time','FontSize',36, 'Interpreter','latex');
ylim([d_lims(1) d_lims(end)])
yticks(d_lims)

print(fig_pos, [dir_to_save_figs 'simu2vita_pos_evo.svg'], "-dsvg");

vel_body = out.vel_body.Data;

u_lims = get_intervals_to_limit_plot(vel_body(:,1), 2, 3);
v_lims = get_intervals_to_limit_plot(vel_body(:,2), 2, 3);
w_lims = get_intervals_to_limit_plot(vel_body(:,3), 2, 3);
p_lims = get_intervals_to_limit_plot(vel_body(:,4), 5, 3);
q_lims = get_intervals_to_limit_plot(vel_body(:,5), 5, 3);
r_lims = get_intervals_to_limit_plot(vel_body(:,6), 5, 3);

fig_vel = figure('units','normalized','outerposition',[0 0 1 1]);
set(fig_vel, 'Visible', 'off');
subplot(3,1,1);
plot(out.vel_body.Time, vel_body(:,1), 'g', 'LineWidth', 3);
set(gca, 'FontSize', 28);
grid on
xlabel('time [s]','FontSize',28);
ylabel('Velocity [m/s]','FontSize',28);
title('$u$ component of {\boldmath$\upsilon$}${}_b$ evolution over time','FontSize',36, 'Interpreter','latex');
ylim([u_lims(1) u_lims(end)])
yticks(u_lims)

subplot(3,1,2);
plot(out.vel_body.Time, vel_body(:,2), 'b', 'LineWidth', 3);
set(gca, 'FontSize', 28);
grid on;
xlabel('time [s]','FontSize',28);
ylabel('Velocity [m/s]','FontSize',28);
title('$v$ component of {\boldmath$\upsilon$}${}_b$ evolution over time','FontSize',36, 'Interpreter','latex');
ylim([v_lims(1) v_lims(end)])
yticks(v_lims)

subplot(3,1,3);
plot(out.vel_body.Time, vel_body(:,3), 'r', 'LineWidth', 3);
set(gca, 'FontSize', 28);
grid on;
xlabel('time [s]','FontSize',28);
ylabel('Velocity [m/s]','FontSize',28);
title('$w$ component of {\boldmath$\upsilon$}${}_b$ evolution over time','FontSize',36, 'Interpreter','latex');
ylim([w_lims(1) w_lims(end)])
yticks(w_lims)

print(fig_vel, [dir_to_save_figs 'simu2vita_vel_evo.svg'], "-dsvg");

fig_gyro = figure('units','normalized','outerposition',[0 0 1 1]);
set(fig_gyro, 'Visible', 'off');
subplot(3,1,1);
plot(out.vel_body.Time, vel_body(:,4), 'g', 'LineWidth', 3);
set(gca, 'FontSize', 28);
grid on
xlabel('time [s]','FontSize',28);
ylabel('Gyros (rpm)','FontSize',28);
title('$p$ component of {\boldmath$\omega$}${}_b$ evolution over time','FontSize',36, 'Interpreter','latex');
ylim([p_lims(1) p_lims(end)])
yticks(p_lims)

subplot(3,1,2);
plot(out.vel_body.Time, vel_body(:,5), 'b', 'LineWidth', 3);
set(gca, 'FontSize', 28);
grid on;
xlabel('time [s]','FontSize',28);
ylabel('Gyros (rpm)','FontSize',28);
title('$q$ component of {\boldmath$\omega$}${}_b$ evolution over time','FontSize',36, 'Interpreter','latex');
ylim([q_lims(1) q_lims(end)])
yticks(q_lims)

subplot(3,1,3);
plot(out.vel_body.Time, vel_body(:,6), 'r', 'LineWidth', 3);
set(gca, 'FontSize', 28);
grid on;
xlabel('time [s]','FontSize',28);
ylabel('Gyros (rpm)','FontSize',28);
title('$r$ component of {\boldmath$\omega$}${}_b$ evolution over time','FontSize',36, 'Interpreter','latex');
ylim([r_lims(1) r_lims(end)])
yticks(r_lims)

print(fig_gyro, [dir_to_save_figs 'simu2vita_gyros_evo.svg'], "-dsvg");

pings = out.pings.Data;

lat_lims = get_intervals_to_limit_plot([pings(:,2); pings(:,3)], 2, 3);

vert_lims = get_intervals_to_limit_plot([pings(:,4); pings(:,5)], 2, 3);

err_lat = pings(:,2) - pings(:,3);
err_vert = pings(:,4) - pings(:,5);
err_lims = get_intervals_to_limit_plot([err_lat; err_vert], 2, 3);

fig_p = figure('units','normalized','outerposition',[0 0 1 1]);
set(fig_p, 'Visible', 'off');
subplot(3,1,1);
plot(out.pings.Time, pings(:,2), 'r--', 'LineWidth', 3);
hold on
plot(out.pings.Time, pings(:,3), 'b', 'LineWidth', 3);
set(gca, 'FontSize', 28);
grid on
legend('$$d_1$$', '$$d_2$$', 'Interpreter','latex', 'Orientation', 'horizontal')
xlabel('time [s]','FontSize',28);
ylabel('Distance [m]','FontSize',28);
title('Lateral Echosounder readings','FontSize',36);
ylim([lat_lims(1) lat_lims(end)])
yticks(lat_lims)

subplot(3,1,2);
plot(out.pings.Time, pings(:,4), 'r--', 'LineWidth', 3);
hold on;
plot(out.pings.Time, pings(:,5), 'b', 'LineWidth', 3);
set(gca, 'FontSize', 28);
grid on;
legend('$$d_3$$', '$$d_4$$', 'Interpreter','latex', 'Orientation', 'horizontal')
xlabel('time [s]','FontSize',28);
ylabel('Distance [m]','FontSize',28);
title('Vertical Echosounder readings','FontSize',36);
ylim([vert_lims(1) vert_lims(end)])
yticks(vert_lims)

subplot(3,1,3);
plot(out.pings.Time, pings(:,2) - pings(:,3), 'r--', 'LineWidth', 3);
hold on
plot(out.pings.Time, pings(:,4) - pings(:,5), 'b', 'LineWidth', 3);
set(gca, 'FontSize', 28);
grid on;
legend('$$e_{sw}$$', '$$e_{he}$$', 'Interpreter','latex', 'Orientation', 'horizontal')
xlabel('time [s]','FontSize',28);
ylabel('Error [m]','FontSize',28);
title('Lateral and Vertical error evolution over time','FontSize',36);
ylim([err_lims(1) err_lims(end)])
yticks(err_lims)

print(fig_p, [dir_to_save_figs 'echosounder_evo.svg'], "-dsvg");

% vel_ned = NED_vel.Data;
% acc_ned = NED_acc_gyros.Data;
% 
% figure('units','normalized','outerposition',[0 0 1 1]);
% subplot(2,1,1);
% plot(NED_vel.Time, vel_ned(:,1), 'g');
% set(gca, 'FontSize', 28);
% grid on;
% xlabel('time [s]','FontSize',28);
% ylabel('Linear Velocity [m/s]','FontSize',28);
% title('Surge velocity w.r.t. NED evolution in time','FontSize',36);
% subplot(2,1,2);
% plot(NED_acc_gyros.Time, acc_ned(:,1), 'b');
% set(gca, 'FontSize', 28);
% grid on;
% xlabel('time [s]','FontSize',28);
% ylabel('Linear acceleration [m/s²]','FontSize',28);
% title('Surge acceleration w.r.t. NED evolution in time','FontSize',36);

thrusters_out = out.thruster_output.Data;
thrusters_in = out.thruster_input.Data;

t_lims = get_intervals_to_limit_plot([thrusters_out(:,1); thrusters_in(:,1)], 2, 6);
td_lims =  get_intervals_to_limit_plot([0.01684 0.04911], 3, 6);

fig_t = figure('units','normalized','outerposition',[0 0 1 1]);
set(fig_t, 'Visible', 'off')
subplot(2,1,1);
plot(out.thruster_output.Time, thrusters_out(:,1), 'b', 'LineWidth', 3);
grid on;
hold on;
plot(out.thruster_input.Time, thrusters_in(:,1), 'r--', 'LineWidth', 3);
set(gca, 'FontSize', 28);
xlabel('time [s]','FontSize',28);
ylabel('Thrust Force [N]','FontSize',28);
title('Evolution of right frontal propeller thrust over time','FontSize',36);
legend('$$y_{a,k}$$', '$$u_{a,k}$$', 'Interpreter','latex', 'Orientation', 'horizontal')
ylim([t_lims(1) t_lims(end)])
yticks(t_lims)

subplot(2,1,2);
plot(out.thruster_output.Time, thrusters_out(:,1), 'b', 'LineWidth', 3);
grid on;
hold on;
plot(out.thruster_input.Time, thrusters_in(:,1), 'r--', 'LineWidth', 3);
set(gca, 'FontSize', 28);
xlabel('time [s]','FontSize',28);
ylabel('Thrust Force [N]','FontSize',28);
xlim([7.2 7.60])
title('Observation of the first order filter effect','FontSize',36);
legend('$$y_{a,k}$$', '$$u_{a,k}$$', 'Interpreter','latex', 'Orientation', 'horizontal')
ylim([td_lims(1) td_lims(end)])
yticks(td_lims)

print(fig_t, [dir_to_save_figs 'simu2vita_prop_evo.svg'], "-dsvg");
