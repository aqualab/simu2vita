function rot_vec = quat_mult (q, r)

    rot_vec = kronnecker_quat(q, r);;
    
end