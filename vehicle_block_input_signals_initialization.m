% init_actuators_state = [0; 0; 1; 0]

u_input = [1; 0.5; 0.7; 0.8]

actuator_group_sizes = [1; 3]

num_actuators = sum(actuator_group_sizes)

groups_time_const = [0.2; 0.1]

groups_alfa = 1./groups_time_const

actuators_tconst_vec = expand_array_by_groups(actuator_group_sizes, groups_alfa)

init_actuators_time_groups = [1; 2; 1]
init_actuators_time_values = [1; 5; 1]

internal_init_actuators_time = expand_array_by_groups(init_actuators_time_groups, init_actuators_time_values)

init_actuators_state_groups = [2; 1; 1]
init_actuators_state_values = [0; 1; 0]

init_actuators_state = expand_array_by_groups(init_actuators_state_groups, init_actuators_state_values)

thrust_max_saturation_groups = [4];
thrust_max_saturation_values = [40];

thrust_max_saturation = expand_array_by_groups(thrust_max_saturation_groups, thrust_max_saturation_values)

thrust_min_saturation_groups = [4];
thrust_min_saturation_values = [-40];

thrust_min_saturation = expand_array_by_groups(thrust_min_saturation_groups, thrust_min_saturation_values)


logic_op_thrust_saturation = true
logic_op_external_alloc_mat = false
logic_op_use_external_time = false
logic_op_use_external_actuators_init_time = false
loigc_op_use_external_forces = false
loigc_op_use_external_current_velocity = false
current_velocity = [0; 0; 0; 0; 0; 0]
nu_0 = [0; 0; 0; 0; 0; 0]
eta_0 = [0; 0; 0; 1; 0; 0; 0]