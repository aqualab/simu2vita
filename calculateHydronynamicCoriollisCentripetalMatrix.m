function Ca = calculateHydronynamicCoriollisCentripetalMatrix(Ma, nu)

    [velRow, velCols] = size(nu);

    if(velRow == 1 || velCols ~= 1)

        ME = MException('MyComponent:incompatiblePosition', ...
                'Position vector does not match a column vector!');

        throw(ME);

    else

        v = Ma*nu;
        
        Smtrx_v1 = [0 -v(3) v(2); ... 
                    v(3) 0 -v(1); ... 
                    -v(2) v(1) 0];

        Smtrx_v2 = [0 -v(6) v(5); ... 
                    v(6) 0 -v(4); ... 
                    -v(5) v(4) 0];

        Ca = [zeros(3,3) -Smtrx_v1; ...
              -Smtrx_v1  -Smtrx_v2]; 


    end

end