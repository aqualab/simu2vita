%==============Velocity Trajectory Generator===============================

sigma_vt = 0.3;
natural_freq_vt = 1;

%velocity trajectory generator

A_vt_2x1 = -natural_freq_vt*natural_freq_vt;
A_vt_2x2 = -2*sigma_vt*natural_freq_vt;

A_vt = [0 1; A_vt_2x1 A_vt_2x2];

B_vt = [0; -A_vt_2x1];

C_vt = eye(2);

D_vt = [0; 0];

vel_sample_freq = 20;

dss_vt = c2d(ss(A_vt, B_vt, C_vt, D_vt), 1/vel_sample_freq, 'tustin');
dA_vt = dss_vt.A;
dB_vt = dss_vt.B;
dC_vt = dss_vt.C;
dD_vt = dss_vt.D;

%==============Position Trajectory Generator===============================

sigma_pt_sway_offset = 5;
sigma_pt_heave_offset = 5;

natural_freq_pt_sway_offset = 0.5;
natural_freq_pt_heave_offset = 0.5;

sigma_pt = diag([sigma_pt_sway_offset; sigma_pt_heave_offset]);
natural_freq_pt = diag([natural_freq_pt_sway_offset; natural_freq_pt_heave_offset]);

natural_freq_pt_sqrd = natural_freq_pt*natural_freq_pt;
natural_freq_pt_cubic = natural_freq_pt_sqrd*natural_freq_pt;

A_pt_b3x1 = -natural_freq_pt_cubic;
A_pt_b3x2 = -(2*sigma_pt + eye(2))*natural_freq_pt_sqrd;
A_pt_b3x3 = -(2*sigma_pt + eye(2))*natural_freq_pt;

A_pt = [zeros(2) eye(2) zeros(2); ...
        zeros(2) zeros(2) eye(2); ...
        A_pt_b3x1 A_pt_b3x2 A_pt_b3x3];

B_pt = zeros(6,2);
B_pt(5:6,1:2) = natural_freq_pt_cubic;

C_pt = eye(6);

D_pt = zeros(6,2);

pos_sample_freq = 4;
dss_pt = c2d(ss(A_pt, B_pt, C_pt, D_pt), 1/pos_sample_freq, 'tustin');
dA_pt = dss_pt.A;
dB_pt = dss_pt.B;
dC_pt = dss_pt.C;
dD_pt = dss_pt.D;

%==============Orientation Trajectory Generator============================
%==============RPY modeling================================================

sigma_ot_roll = 20;
sigma_ot_pitch = 20;
sigma_ot_yaw = 20;

natural_freq_ot_roll = 1;
natural_freq_ot_pitch = 1;
natural_freq_ot_yaw = 1;

sigma_ot = diag([sigma_ot_roll; sigma_ot_pitch; sigma_ot_yaw]);
natural_freq_ot = diag([natural_freq_ot_roll; natural_freq_ot_pitch; natural_freq_ot_yaw]);

natural_freq_ot_sqrd = natural_freq_ot*natural_freq_ot;
natural_freq_ot_cubic = natural_freq_ot_sqrd;
A_ot_b3x1 = -natural_freq_ot_cubic*natural_freq_ot;

A_ot_b3x1 = -natural_freq_ot_cubic;
A_ot_b3x2 = -(2*sigma_ot + eye(3))*natural_freq_ot_sqrd;
A_ot_b3x3 = -(2*sigma_ot + eye(3))*natural_freq_ot;

A_ot = [zeros(3) eye(3) zeros(3); ...
        zeros(3) zeros(3) eye(3); ...
        A_ot_b3x1 A_ot_b3x2 A_ot_b3x3];

B_ot = zeros(9,3);
B_ot(7:9,1:3) = natural_freq_ot_cubic;

C_ot = eye(9);
        
D_ot = zeros(9,3);

ori_sample_freq = 20;
dss_ot = c2d(ss(A_ot, B_ot, C_ot, D_ot), 1/ori_sample_freq, 'tustin');
dA_ot = dss_ot.A;
dB_ot = dss_ot.B;
dC_ot = dss_ot.C;
dD_ot = dss_ot.D;

%==============Quaternion time law modeling================================

sigma_ot_q = 1;
natural_freq_ot_q = 0.1;

natural_freq_ot_q_sqrd = natural_freq_ot_q*natural_freq_ot_q;
natural_freq_ot_q_cubic = natural_freq_ot_q_sqrd*natural_freq_ot_q;

A_ot_q_3x1 = -natural_freq_ot_q_cubic;
A_ot_q_3x2 = -(2*sigma_ot_q + 1)*natural_freq_ot_q_sqrd;
A_ot_q_3x3 = -(2*sigma_ot_q + 1)*natural_freq_ot_q;


A_ot_q = [0 1 0; ...
          0 0 1; ...
          A_ot_q_3x1 A_ot_q_3x2 A_ot_q_3x3];
          
B_ot_q = [0 0 natural_freq_ot_q_cubic]';
C_ot_q = eye(3);
D_ot_q = zeros(3,1);

ori_sample_freq_q = 20;
dss_ot_q = c2d(ss(A_ot_q, B_ot_q, C_ot_q, D_ot_q), 1/ori_sample_freq_q, 'tustin');
dA_ot_q = dss_ot_q.A;
dB_ot_q = dss_ot_q.B;
dC_ot_q = dss_ot_q.C;
dD_ot_q = dss_ot_q.D;

%==============Axis-Angle modeling=========================================

sigma_ot_angle = 0.707;
natural_freq_ot_angle = 1;

natural_freq_ot_angle_sqrd = natural_freq_ot_angle*natural_freq_ot_angle;
natural_freq_ot_angle_cubic = natural_freq_ot_angle_sqrd*natural_freq_ot_angle;

A_ot_angle_3x1 = -natural_freq_ot_angle_cubic;
A_ot_angle_3x2 = -(2*sigma_ot_angle + 1)*natural_freq_ot_angle_sqrd;
A_ot_angle_3x3 = -(2*sigma_ot_angle + 1)*natural_freq_ot_angle;


A_ot_angle = [0 1 0; ...
              0 0 1; ...
              A_ot_angle_3x1 A_ot_angle_3x2 A_ot_angle_3x3];
          
B_ot_angle = [0 0 natural_freq_ot_angle_cubic]';
C_ot_angle = eye(3);
D_ot_angle = zeros(3,1);

sigma_ot_x_axis = 1;
sigma_ot_y_axis = 1;
sigma_ot_z_axis = 1;

natural_freq_ot_x_axis = 20;
natural_freq_ot_y_axis = 20;
natural_freq_ot_z_axis = 20;

sigma_ot_axis = diag([sigma_ot_x_axis; ...
                 sigma_ot_y_axis; ...
                 sigma_ot_z_axis]);

natural_freq_ot_axis = diag([natural_freq_ot_x_axis; ...
                        natural_freq_ot_y_axis; ...
                        natural_freq_ot_z_axis;]);

natural_freq_ot_sqrd_axis = natural_freq_ot_axis*natural_freq_ot_axis;
natural_freq_ot_cubic_axis = natural_freq_ot_sqrd_axis*natural_freq_ot_axis;

A_ot_axis_b3x1 = -natural_freq_ot_cubic_axis;
A_ot_axis_b3x2 = -(2*sigma_ot_axis + eye(3))*natural_freq_ot_sqrd_axis;
A_ot_axis_b3x3 = -(2*sigma_ot_axis + eye(3))*natural_freq_ot_axis;

A_ot_axis = [zeros(3) eye(3) zeros(3); ...
             zeros(3) zeros(3) eye(3); ...
             A_ot_axis_b3x1 A_ot_axis_b3x2 A_ot_axis_b3x3];

B_ot_axis = zeros(9,3);
B_ot_axis(7:9,1:3) = natural_freq_ot_cubic_axis;

C_ot_axis = eye(9);

D_ot_axis = zeros(9,3);

error_filter_cut_freq = natural_freq_ot_x_axis;
error_derivative_limit_freq = 10;
error_Kp = 100;
error_Ki = 2*0.707*100;